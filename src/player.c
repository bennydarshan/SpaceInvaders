#include <SDL2/SDL.h>
#include <stdlib.h>

struct player{
	SDL_Rect *body;
	SDL_Rect *tex;
	int cooldown;
};

struct bullet{
	SDL_Rect *body;
	struct bullet *next;
};

struct player *player_init()
{
	struct player *ret;
	ret = malloc(sizeof(struct player));
	ret->body = malloc(sizeof(SDL_Rect));

	ret->body->h = 32;
	ret->body->w = 64;
	ret->body->x = 800/2;
	ret->body->y = 600 - ret->body->h;
	ret->cooldown = 11;
	return ret;

}

void player_draw(struct player *plr, SDL_Renderer *ren)
{
	//TODO: make it a texture
	SDL_RenderDrawRect(ren, plr->body);
}

void player_move(struct player *plr, unsigned char dir)
{

	//-1 - not moving; 0 - right; 1 - left;
	switch(dir)
	{
	case 1:
		plr->body->x-=5;
		break;
	case 0:
		plr->body->x+=5;
		break;
	}
	plr->cooldown++;
}

struct bullet *bullet_init()
{
	struct bullet *ret;
	ret = malloc(sizeof(struct bullet));
	ret->body = NULL;
	ret->next = NULL;
	return ret;
}

void player_shoot(struct player *plr, struct bullet *bullets)
{
	struct bullet *new;
	if(plr->cooldown > 30)
	{
/*		if(bullets == NULL)
		{ //FIXME: not needed anymore
			bullets = malloc(sizeof(struct bullet));
			bullets->body = malloc(sizeof(SDL_Rect));
			bullets->body->x = plr->body->x + plr->body->w/2;
			bullets->body->y = plr->body->y;
			bullets->body->w = 4;
			bullets->body->h = 8;

		}*/
		new = malloc(sizeof(struct bullet));
		new->next = NULL;
		new->body = malloc(sizeof(SDL_Rect));
		new->body->x = plr->body->x + plr->body->w/2;
		new->body->y = plr->body->y;
		new->body->w = 4;
		new->body->h = 8;
		while(bullets->next)
			bullets = bullets->next;
		bullets->next = new;
		plr->cooldown = 0;
	}
}

SDL_Rect *player_body(struct player *plr)
{
	return plr->body;
}

void player_kill(struct player *plr)
{
	//TODO: reduce life.
	plr->body->x = 800/2;
}

void bullet_move(struct bullet *bullets)
{
	bullets = bullets->next;
	while(bullets)
	{
		bullets->body->y-=5;
		bullets = bullets->next;
	}
}

void bullet_draw(struct bullet *bullets, SDL_Renderer *ren)
{
	bullets= bullets->next;
	while(bullets)
	{
		SDL_RenderFillRect(ren, bullets->body);
		bullets = bullets->next;
	}

}

int bullet_hit(struct bullet *bullets, SDL_Rect *r)
{
	if(!r) return 0;
	struct bullet *temp;
	temp = bullets;
	bullets = bullets->next;
	while(bullets)
	{
		if((bullets->body->y < r->y+r->h) && (bullets->body->y + bullets->body->h > r->y) && (bullets->body->x + bullets->body->w > r->x) && (bullets->body->x < r->x + r->w))
		{
			temp->next = bullets->next;
			free(bullets->body);
			free(bullets);
			bullets = temp;
			return 1;

		}
		temp = bullets;
		bullets = bullets->next;
	}
	return 0;
}
