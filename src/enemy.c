#include<stdio.h>
#include<stdlib.h>

#include<SDL2/SDL.h>

struct enemy{
	SDL_Rect *body;
	SDL_Rect *tex;
	int type;
};

struct bolt{
	SDL_Rect *body;
	SDL_Rect *tex;
	struct bolt *next;
};

struct enemy enemy_init(int x, int y, int type, SDL_Rect **texs)
{
	struct enemy ret;
	ret.body = malloc(sizeof(SDL_Rect));
	ret.body->x = x;
	ret.body->y = y;
	ret.body->h = 32;
	ret.body->w = 32;
	ret.type = type;
	ret.tex = texs[type%3];

	return ret;

}

SDL_Rect **texs()
{
	SDL_Rect **ret;
	ret = malloc(sizeof(SDL_Rect *) * 3);
	for(int i = 0; i < 3; i++)
		ret[i] = malloc(sizeof(SDL_Rect)*2);

	ret[0][0].x = 11;
	ret[0][0].y = 9;
	ret[0][0].h = 95;
	ret[0][0].w = 119;
	ret[0][1].x = 130;
	ret[0][1].y = 0;
	ret[0][1].h = 115;
	ret[0][1].w = 150;

	ret[1][0].x = 300;
	ret[1][0].y = 0;
	ret[1][0].w = 130;
	ret[1][0].h = 115;
	ret[1][1].x = 280;
	ret[1][1].y = 0;
	ret[1][1].w = 130;
	ret[1][1].h = 115;

	ret[2][0].x = 0;
	ret[2][0].y = 115;
	ret[2][0].h = 115;
	ret[2][0].w = 130;
	ret[2][1].x = 140;
	ret[2][1].y = 115;
	ret[2][1].h = 115;
	ret[2][1].w = 130;
	return ret;
}

struct enemy **enemies_init()
{
	struct enemy **ret;
	SDL_Rect **tex;
	tex = texs();
	ret = malloc(sizeof(struct enemy*)*5);
	for (int i = 1; i < 7; i++)
	{
		ret[i-1] = malloc(sizeof(struct enemy)*11);
		for (int j = 0; j < 11; j++)
			ret[i-1][j] = enemy_init(32 + 64*j, 32 + 64*(i-1), i, tex);
	}
	return ret;
}



struct enemy most_bottom(struct enemy **enemies, int y)
{
	for(int i = 5;i >= 0; i--)
		if(enemies[i][y].type != 0) return enemies[i][y];
}

struct enemy most_right(struct enemy **enemies)
{
	for (int i = 10; i >=0 ; i--)
	{
	    for(int j = 0; j < 6; j++)
			if(enemies[j][i].type != 0) return enemies[j][i];
	}
}
struct enemy most_left(struct enemy **enemies)
{
	for (int i = 0; i < 11 ; i++)
	{
	    for(int j = 0; j < 6; j++)
		{
			if(enemies[j][i].type != 0)
				return enemies[j][i];
		}
	}
}

void enemies_moveright(struct enemy **enemies)
{
	for (int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 11; j++)
		{
			enemies[i][j].body->x+=5;
		}
	}
}

void enemies_moveleft(struct enemy **enemies)
{
	for (int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 11; j++)
		{
			enemies[i][j].body->x-=5;
		}
	}
}

void enemies_movedown(struct enemy **enemies)
{
	for (int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 11; j++)
		{
			enemies[i][j].body->y+=5;
		}
	}
}
void enemies_move(struct enemy **enemies, unsigned char *dir)
{
	//0 - right; 1 - left

	static int delay = 0;
	most_right(enemies);
	if((*dir) && (most_left(enemies).body->x < 0))
	{
		enemies_movedown(enemies);
		*dir = 0;
		return;
	}
	else if(!(*dir) && (most_right(enemies).body->x + most_right(enemies).body->w > 800))
	{
		enemies_movedown(enemies);
		*dir = 1;
		return;
	}
	if(delay >= 20)
	{
		delay = 0;
		if(*dir) enemies_moveleft(enemies);
		else enemies_moveright(enemies);
	}
	delay++;

}

void enemy_render(struct enemy nmy, SDL_Renderer *ren, SDL_Texture *tex)
{
	//TODO:change it to a texture.
	if(nmy.type !=0)
		SDL_RenderCopy(ren, tex, nmy.tex+1, nmy.body);
}

void enemies_render(struct enemy **enemies, SDL_Renderer *ren, SDL_Texture *tex)
{
	for (int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 11; j++)
		{
			enemy_render(enemies[i][j], ren, tex);
		}
	}
}

SDL_Rect *enemies_getBody(struct enemy **enemies,int x, int y)
{
	if(enemies[x][y].type != 0)
		return enemies[x][y].body;
	return NULL;
}

void *enemy_kill(struct enemy **enemies, int x, int y)
{
	enemies[x][y].type = 0;
}

struct bolt *enemy_shoot(struct enemy **enemies)
{
	struct bolt *ret;
	struct enemy nmy= most_bottom(enemies, rand()%6);
	ret = malloc(sizeof(struct bolt));
	ret->next = NULL;
	ret->body = malloc(sizeof(SDL_Rect));
	ret->body->y = nmy.body->y + nmy.body->h;
	ret->body->x = nmy.body->x + nmy.body->w/2;
	ret->body->w = 8;
	ret->body->h = 16;

	return ret;
}

void enemies_shoot(struct enemy **enemies, struct bolt *bolts)
{
	static int delay = 0;
	if((delay > 150) && (rand()%30==0))
	{
		while(bolts->next)
			bolts = bolts->next;
		bolts->next = enemy_shoot(enemies);
		delay = 0;
	}
	delay++;
}

struct bolt *bolt_init()
{
	struct bolt *ret;
	ret = malloc(sizeof(struct bolt));
	ret->body = NULL;
	ret->tex = NULL;
	ret->next = NULL;
	return ret;
}

void bolt_draw(struct bolt *bolts, SDL_Renderer *ren)
{
	//TODO: make it a texture
	bolts = bolts->next;
	while(bolts)
	{
		SDL_RenderFillRect(ren, bolts->body);
		bolts = bolts->next;
	}
}

void bolt_move(struct bolt *bolts)
{
	bolts = bolts->next;
	while(bolts)
	{
		bolts->body->y+=5;
		bolts = bolts->next;
	}

}

int bolt_hit(struct bolt *bullets, SDL_Rect *r)
{ //TODO: can use a name change.
	if(!r) return 0;
	struct bolt *temp;
	temp = bullets;
	bullets = bullets->next;
	while(bullets)
	{
		if((bullets->body->y < r->y+r->h) && (bullets->body->y + bullets->body->h > r->y) && (bullets->body->x + bullets->body->w > r->x) && (bullets->body->x < r->x + r->w))
		{
			temp->next = bullets->next;
			free(bullets->body);
			free(bullets);
			bullets = temp;
			return 1;

		}
		temp = bullets;
		bullets = bullets->next;
	}
	return 0;
}

