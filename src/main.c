/* spaceinvaders.c
 *
 * Copyright 2018 bennydarshan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <time.h>

#include "enemy.h"
#include "player.h"



int
main (int argc,
      char *argv[])
{
	SDL_Window *win;
	SDL_Renderer *ren;
	SDL_Event e;
	SDL_Surface *stex;
	SDL_Texture *textures;
	unsigned char running = 1;

	struct enemy **enemies;
	unsigned char side = 0;
	struct bolt *bolts;

	struct player *plr;
	unsigned char dir = -1;
	struct bullet *bullets;

	//DEBUG
	SDL_Rect r = {11, 9, 130, 115};

	SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO);
	srand(time(NULL));

	win = SDL_CreateWindow("Space Invaders", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_OPENGL);
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
	stex = SDL_LoadBMP("img/sprites.bmp");
	textures = SDL_CreateTextureFromSurface(ren, stex);

	enemies = enemies_init();
	plr = player_init();
	bullets = bullet_init();
	bolts = bolt_init();



	while(running)
	{
		while(SDL_PollEvent(&e))
		{
			switch(e.type)
			{
			case SDL_QUIT:
				running = 0;
				break;
			case SDL_KEYDOWN:
				switch(e.key.keysym.sym)
				{
				case SDLK_RIGHT:
					dir = 0;
					break;
				case SDLK_LEFT:
					dir = 1;
					break;
				case SDLK_SPACE:
					player_shoot(plr, bullets);
				}
				break;
			case SDL_KEYUP:
				switch(e.key.keysym.sym)
				{
				case SDLK_RIGHT:
					if(dir == 0) dir = -1;
					break;
				case SDLK_LEFT:
					if(dir == 1) dir = -1;
					break;
				}

				break;
			}
		}

		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);
		SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);

		enemies_move(enemies, &side);
		enemies_shoot(enemies, bolts);
		player_move(plr, dir);
		bullet_move(bullets);
		bolt_move(bolts);

		for(int i = 0; i < 6; i++)
			for(int j = 0; j < 11; j++)
			{
				if(bullet_hit(bullets, enemies_getBody(enemies, i, j)))
					enemy_kill(enemies, i, j);
			}
		if(bolt_hit(bolts, player_body(plr)))
		{
			player_kill(plr);
			SDL_Delay(1000);
		}

		enemies_render(enemies, ren, textures);
		//SDL_RenderCopy(ren, textures, &r, &r);
		player_draw(plr, ren);
		bullet_draw(bullets, ren);
		bolt_draw(bolts, ren);
		SDL_RenderPresent(ren);

		SDL_Delay(1000/30);
	}

	SDL_DestroyWindow(win);
	SDL_DestroyRenderer(ren);
	printf ("Hello, World!\n");
	return EXIT_SUCCESS;
}
