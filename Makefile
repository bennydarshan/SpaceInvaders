all: spaceinvaders

WARNINGS = -Wall
IDIR = -Iinclude
SRC = $(shell find src/*.c)
OBJ = $(subst src,dist,$(SRC:.c=.o))
DEBUG = -ggdb -fno-omit-frame-pointer
CLIBS = -lSDL2
OPTIMIZE = -O2

spaceinvaders: $(OBJ)
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $^ $(CLIBS)

dist/%.o: src/%.c
	$(CC) -c -o $@ $^ $(DEBUG) $(WARNINGS) $(OPTIMIZE) $(IDIR)

clean:
	rm -f spaceinvaders $(OBJ)

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./spaceinvaders

