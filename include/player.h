#include<SDL2/SDL.h>

struct player;
struct bullet;

struct player *player_init ();
void player_draw (struct player *plr, SDL_Renderer  *ren);
void player_move(struct player *plr, unsigned char dir);
void player_shoot(struct player *plr, struct bullet *bullets);
void player_kill(struct player *plr);
SDL_Rect *player_body(struct player *plr);

struct bullet *bullet_init();
void bullet_draw(struct bullet *bullets, SDL_Renderer *ren);
void bullet_move(struct bullet *bullets);
int bullet_hit(struct bullet *bullets, SDL_Rect *r);
