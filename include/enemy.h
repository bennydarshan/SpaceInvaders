#include<SDL2/SDL.h>

struct enemy;
struct bolt;


struct enemy **enemies_init();
void enemies_render(struct enemy **enemies, SDL_Renderer *ren, SDL_Texture *tex);
void enemies_move(struct enemy **enemies, unsigned char *dir);
SDL_Rect *enemies_getBody(struct enemy **enemies,int x, int y);
void *enemy_kill(struct enemy **enemies, int x, int y);
void enemies_shoot(struct enemy **enemies, struct bolt *bolts);

struct bolt *bolt_init();
void bolt_draw(struct bolt *bolts, SDL_Renderer *ren);
void bolt_move(struct bolt *bolts);
int bolt_hit(struct bolt *bullets, SDL_Rect *r);
